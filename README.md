## Zander's Brainfuck Compiler

Compiles brainfuck to C code
and relies on a little linked list library to get the job done on the
runtime end.

Run the Makefile (using GNU make) to compile the compiler, 
and then 'bfcomp' should appear in the build directory. Run that
with the input file (source) and give it a corresponding output file to
write to, and the output file should be a native executable which does
the tasks outlined in the brainfuck source.

Example:

```
./bfcomp rot13.bf rot13

file rot13
ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked...
```

To-do:

* Linting (detect errors before compilation)
* Use linked-list-based stack to avoid having arbitrary limited nesting depth
* Provide option to use array-based memory instead of linked-list-based for runtime (DONE)
* Implement help page and more fun command-line options

Fun stuff.
