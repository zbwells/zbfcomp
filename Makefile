default:
	mkdir build
	mkdir build/lib
	cp src/llist.h build/lib/llist.h
	cp src/array.h build/lib/array.h
	cc -c src/llist.c -o build/lib/llist.o
	cc -c src/array.c -o build/lib/array.o
	cc src/main.c -o build/translator
	cp src/bfcomp.sh build/bfcomp
	chmod +x build/bfcomp
	
clean:
	rm -r build
