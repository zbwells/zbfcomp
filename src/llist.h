/* SPDX-License-Identifier: CC0-1.0 */

#pragma once

/* 
 * zbwells
 * Sun Dec 24
 */

/* Node for doubly linked list */
typedef struct node {
    char datum;
    struct node *right;
    struct node *left;        
} node;

/* Functions which do things with the doubly linked list */
void initialize_list(node *);
void pointer_left(node **);
void pointer_right(node **);
void put_byte(node *);
void get_byte(node **);
void inc_val(node *);
void dec_val(node *);
char get_current(node *);
