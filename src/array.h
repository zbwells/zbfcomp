/* SPDX-License-Identifier: CC0-1.0 */

#pragma once

/* 
 * zbwells
 * Sun Dec 24
 */

void initialize_list(char **, size_t);
void pointer_right(char **);
void pointer_left(char **);
void put_byte(char *);
void get_byte(char **);
void inc_val(char *);
void dec_val(char *);
char get_current(char *);
