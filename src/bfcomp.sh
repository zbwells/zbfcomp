# SPDX-License-Identifier: CC0-1.0

## Shell script that handles the brainfuck compilation process
## by wrapping a C program.
## zbwells
## Sun Dec 24

# terminate if any errors occur
set -e

if test -z "$1" || test -z "$2"; then
    echo "Usage: bfcomp <infile> <outfile> [flags]" >&2
    exit 1
fi

if test "$3" == "-a"; then
    LIBRARY="lib/array.o"
else
    LIBRARY="lib/llist.o"
fi

"$(dirname $0)"/translator $1 $2.c $3 $4


cc $LIBRARY $2.c -o $2
