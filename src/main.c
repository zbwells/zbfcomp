/* SPDX-License-Identifier: CC0-1.0 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
 * Compiles Brainfuck code to C code, which can then later
 * be compiled to an actual executable.
 * zbwells
 * Sat Dec 23
 */

/* write all of the things usual for a simple C program, but no malloc */
void init_file_array(FILE *out, size_t arrsize)
{
    char *initialstr = "#include <stdio.h>\n"
                       "#include <stdlib.h>\n"
                       "#include \"lib/array.h\"\n\n"
                       "int main(void) {\n"
                       "\tchar *tape;\n"
                       "\tinitialize_list(&tape, %d);\n";

    fprintf(out, initialstr, arrsize);        
}

/* write all of the things usual for a simple C program, linking a lib */
void init_file_llist(FILE *out)
{
    char *initialstr = "#include <stdio.h>\n"
                       "#include <stdlib.h>\n"
                       "#include \"lib/llist.h\"\n\n"
                       "int main(void) {\n"
                       "\tnode *tape = (node *)malloc(sizeof(struct node));\n"
                       "\tinitialize_list(tape);\n";

    fprintf(out, initialstr);
}

/* Parse bf file and write C code output to file */
void bfparse(FILE *in, FILE *out)
{
    char ch;
    long opcounter = 0;
    long jmplabels_arr[1028] = { 0 };
    long *jmplabels = jmplabels_arr;
    
    while ((ch = fgetc(in)) != EOF) {
        opcounter++;
        switch (ch) {
            case '+':
                fprintf(out, "\tinc_val(tape);\n");
                break;
            case '-':
                fprintf(out, "\tdec_val(tape);\n");
                break;
            case '>':
                fprintf(out, "\tpointer_right(&tape);\n");
                break;
            case '<':
                fprintf(out, "\tpointer_left(&tape);\n");
                break;
            case '.':
                fprintf(out, "\tput_byte(tape);\n");
                break;
            case ',':
                fprintf(out, "\tget_byte(&tape);\n");
                break;
            case '[':
                *jmplabels = opcounter; 
                fprintf(out, "aL%i:\n", *jmplabels);
                fprintf(out, "\tif (get_current(tape) == 0) goto bL%i;\n", *jmplabels);
                jmplabels++;
                break;
            case ']':
                jmplabels--;                
                fprintf(out, "\tif (get_current(tape) != 0) goto aL%i;\n", *jmplabels); 
                fprintf(out, "bL%i:\n", *jmplabels);
                break;
            default:
                break;
        }                
    }

    fprintf(out, "}\n");
}       

int main(int argc, char **argv)
{
    FILE *output;
    FILE *input;
    char *outfilename;
    char *infilename;
    size_t tape_size = 0;
    
    /* just temporary arg handling for now */
    if (argc > 2) {
        infilename = argv[1];
        outfilename = argv[2];
    } else {
        fprintf(stderr, "Usage: bfcomp <infile> <outfile> [flags]\n");
        return 0;
    }

    if (argc > 3 && strcmp("-a", argv[3]) == 0) {
        if (argc > 4) {
            tape_size = atoi(argv[4]);
        } else {
            fprintf(stderr, "Using default tape length of 1024 bytes.\n");
            tape_size = 1024;
        }     
    } 
    

    /* open input file */
    input = fopen(infilename, "r");
    if (input == NULL) {
        fprintf(stderr, "Err: unable to open input file.\n");
        return 1;
    }

    /* open output file */
    output = fopen(outfilename, "w");
    if (input == NULL) {
        fprintf(stderr, "Err: unable to open output file.\n");
        return 2;
    }

    /* Check to see which type of memory to use */
    if (tape_size > 0) {
        init_file_array(output, tape_size);    
    } else {
        init_file_llist(output);
    }
    
    /* parse bf file to write C code */
    bfparse(input, output); 

    /* wrap things up; not strictly necessary, just good practice */
    fclose(output);
    fclose(input);   
}
